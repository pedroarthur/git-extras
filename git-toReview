#!/bin/bash

#
# Default values
#
currentBranch=$(git currentBranch)

commit="HEAD"
targetRef="refs/for/"

branch=$(git config branch.${currentBranch}.merge)
remote=$(git config branch.${currentBranch}.remote)

git=git

#
# Help
#
show_help () {
    cat  <<EOF
Usage: git-toReview [-hxd] [-b branch] [-r remote] [-c commit]

Utility to send commits to review

Options:
    -h               show this message and exit
    -x               print the resulting git command and exit
    -d               toggle send as draft
    -b brach         set the branch of the review
    -r remote        set the remote of the review
    -c commit        set the commit to review
EOF

    exit ${1:-0}
}

#
# Reading the CLI arguments
#
while getopts ":dxb:r:c:" opt
do
    case $opt in
        h) show_help ;;
        x) git="echo git" ;;
        d) targetRef="refs/drafts/" ;;

        b) branch=$OPTARG ;;
        r) remote=$OPTARG ;;
        c) commit="$(git rev-parse --verify "$OPTARG")" || exit 126 ;;

        \?) show_help 255 ;;
    esac
done

#
# Making sure everything is OK
#
if [ -z "${branch}" ]
then
    echo -e "# Can't find review branch configuration"
    echo -e "# please set your upstream"

    exit 128
fi

if [ -z "${remote}" ]
then
    echo -e "# Can't find remote host configuration"
    echo -e "# please set your upstream"

    exit 64
fi

#
# Compiling data
#
fullTarget="${commit}:${targetRef}${branch}"

#
# Some information to users to our users
#
echo -e "# Sending ${commit} to review"
echo -e "# Using ${remote} as review target"
echo -e "# Using ${branch} as review branch\n"

#
# Getting things done
#
$git push "${remote}" "${fullTarget}" || exit $?
$git journal a "${commit} was sent to review at ${remote}"

