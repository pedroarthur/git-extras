#!/bin/bash

# pedroarthur @ 2012-07-13 / 2012-07-31 15:26 pedro.duarte

CS_CONF="/usr/share/datacom/dmview-dev/checkstyle/checkstyle-20141013.xml"

operatingSystem=$(uname -o)

errorSignalizerLink="/tmp/checkstyle.error.last.txt"

csFlags="-c"
actualCheckstyle="$(which checkstyle 2> /dev/null)"

if [ $? -ne 0 ]
then
    if [ ! -e "$CHECKSTYLE" ]
    then
        echo "Could not find checkstyle binary"
        exit 8
    elif [ ! -x "$CHECKSTYLE" ]
    then
        echo "checkstyle is not executable"
        exit 16
    fi

    actualCheckstyle="$CHECKSTYLE"
    csFlags=""
fi

if [ ! -e "$CS_CONF" ]
then
    echo "Could not find checkstyle configuration file"
    exit 32
fi

# 1 desativa debug
# 0 ativa
debug () {
    return ${DEBUG:-1}
}

isCygwin () {
    if [ $1 == "Cygwin" ]
    then
        return 0
    fi

    return 1
}

callCheckStyle () {
    "$actualCheckstyle" "$csFlags" "$($normalizer $CS_CONF)" "$1" >& "$outfile"

    if [ $? -ne 0 ]
    then
        cat "$outfile"
        exit 0
    elif debug
    then
        echo -e "\n > Checkstyle output follows"
        checkstyleOutputNormalizer "$outfile"
        echo -e " > End of Checkstyle Output\n"
    fi

    if isCygwin $operatingSystem
    then
        dos2unix "$outfile" >& /dev/null
    fi
}

checkstyleOutputNormalizer () {
    egrep -v '(log4j:WARN)|(Audit done.$)|(Starting audit...)' "$1"
}

extractLineNumber () {
    sed -e "s?$1:??" -e 's?:.*??' -e '/[^0-9]\+/d' <<< "$2"
}

if isCygwin $operatingSystem
then
    debug && echo "Setting Cygwin data normalizers"

    normalizer="cygpath -w "
    sedNormalizer="sed s?\\\\?/?g "
    decoder="iconv -f WINDOWS-1252 -t UTF-8 "
else
    debug && echo "Using data pass-throughs"

    normalizer="echo "
    sedNormalizer="cat "
    decoder="cat "
fi

echo "Starting checkstyle analysis..."

if [ ${#@} -gt 0 ]
then
    lastHash=$1
else
    lastHash=$(git mylastcommit)
fi

debug && echo "target will be ${lastHash}"

errorSignalizerFile="/tmp/checkstyle.error.${lastHash}.txt"
outfile="/tmp/checkstyle.out.${lastHash}.txt"

if [ -e $errorSignalizerFile ]
then
    rm "$errorSignalizerFile"
fi

# Am I going to inspect stage or a specific revision?
if [ "$lastHash" != "--staged" ]
then # A specific revision? Then I will need a baselne
    if git rev-parse --verify "$lastHash~1" &> /dev/null
    then
        previousHash="$lastHash~1"
    else
        previousHash="$(git emptyobject)"
    fi

    diffRange="$previousHash".."$lastHash"

    currentBranch="$(git symbolic-ref -q HEAD)"
    currentBranch=${currentBranch##refs/heads/}
    currentBranch=${currentBranch:-HEAD}

    debug && echo "Current branch is $currentBranch"

    # Will clean the shit!
    trap "git unstashingCheckout $currentBranch 2> /dev/null" SIGINT SIGTERM EXIT
    # Will change branch to avoid false-positives
    git stashedCheckout "$lastHash" &> /dev/null ||
        (git checkout "$currentBranch" && exit 1)
else
    diffRange=$lastHash
fi

mkdir /tmp/git-checkstyle/ &> /dev/null

verifyChecksum () {
    if [ -e "$1" ]
    then
        [ -s "$1" ] && cat "$1" >> "$errorSignalizerFile"

        return 0
    else
        > "$1"
    fi

    return 1
}

git diff "$diffRange" --name-only --diff-filter=AMR --color=never |
egrep '.java$' | while read fname
do
    r=0

    rangesStart=()
    rangesEnd=()

    echo " * Checking $fname"

    normalizedFname="$($normalizer $(pwd)/$fname)"
    sedFriendlyFileName="$($sedNormalizer <<< $normalizedFname)"

    checksum="$(sha1sum "$normalizedFname" | awk '{print $1}')"
    checksumFileName="/tmp/git-checkstyle/$checksum"

    echo " ~ Output will be written to $checksumFileName"

    if verifyChecksum "$checksumFileName"
    then
        echo -e " - Using data from a $checksumFileName\n" && continue
    fi

    callCheckStyle "$normalizedFname" & csPid=$!

    minRange=$(( 2 ** 32 ))
    maxRange=0

    for rawRange in $(git diff --color=never -U0 $diffRange $fname |
        egrep '^@@' | awk '{print $3 }')
    do

        range=$(sed -e 's:[+,]: :g' <<< "$rawRange")

        rangeStart=$(awk '{print $1}' <<< $range)
        rangeEndOff=$(awk '{print $2}' <<< $range)

        rangesStart[$r]=$rangeStart
        rangesEnd[$r]=$((rangeStart + ${rangeEndOff:-0} ))

        debug &&
            echo " = Found a change ranging from ${rangesStart[$r]} to ${rangesEnd[$r]}"

        # Otimização: se saída do checkstyle está além do range mínimo e máximo
        # da minha mudança, então a violação não é minha
        [ ${rangesStart[$r]} -le $minRange ] && minRange=${rangesStart[$r]}
        [ ${rangesEnd[$r]} -ge $maxRange ] && maxRange=${rangesEnd[$r]}

        r=$((r+1))
    done

    debug && echo " = minRange is $minRange"
    debug && echo " = maxRange is $maxRange"

    wait $csPid

    while IFS=$'\n' read cline
    do
        lnum=$(extractLineNumber "$sedFriendlyFileName" "$cline")

        # Aplicando otimização do range mínimo e máximo
        [ $lnum -lt $minRange ] && continue
        [ $lnum -gt $maxRange ] && continue

        echo -n " - Checking if the warning at line $lnum might be yours... "

        found=0
        currentlyFound=$found

        # Binary search
        rangeRoof=${#rangesStart[@]}
        rangeFloor=0

        while [ $rangeFloor -lt $rangeRoof ]
        do
            i=$(( ($rangeFloor + $rangeRoof) / 2 ))

            if [ $lnum -ge ${rangesStart[$i]} ]
            then
                if [ $lnum -le ${rangesEnd[$i]} ]
                then
                    echo "YES, IT MAY!"
                    found=$((found+1))
                    echo "$cline" >> "$checksumFileName"
                    echo "$cline" >> "$errorSignalizerFile"

                    break
                fi

                rangeFloor=$((i + 1))
            else
                rangeRoof=$((i - 1))
            fi
        done

        [ $found -eq $currentlyFound ] && echo No


    done < <($sedNormalizer "$outfile" | # All this is non-POSIX
    egrep "$sedFriendlyFileName" | $decoder)

    echo ""

    mv "$outfile" "$checksumFileName".txt
done

# Trap faz voltar ao branch!
# git checkout "$currentBranch" &> /dev/null

# Apagando o link antigo. Caso não haja violações na ultima verificação,
# o link simbólico não existirá.
rm "$errorSignalizerLink" &> /dev/null

if [ -e "$errorSignalizerFile" ]
then
    ln -s "$errorSignalizerFile" "$errorSignalizerLink"

    echo -e "\nPlease, before pushing your changes consider solving the warnings"
    echo "aforementioned or explaining why they should exist in the codebase."
    echo "Also, we humbly ask you to consider analysing other bugs that may"
    echo "exist in those files. Sincerely,"
    echo "                                               DmView's QA Team"

    echo -e "\nCheckstyle output written to \e[1;31m$errorSignalizerFile\e[0m. For"
    echo -e "your convenience, \e[1;37m$errorSignalizerLink\e[0m is pointing to that"
    echo -e 'file. May the Force be with you... Always!\n'

    read -p "Do you wish to see the resulting violations file? [y/N] " wannaSeeIt

    if [ "${wannaSeeIt:-n}" == "y" ]
    then
        git lastCheckstyle
    fi

    exit 1
fi

echo "Checkstyle analysis finished"
exit 0

