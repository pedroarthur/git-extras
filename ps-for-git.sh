#!/bin/bash

randomlyShowFortune () {
    if (( ((RANDOM % 100)) >= 95 ))
    then
        echo -ne "\n"
        fortune -n 130 -s | awk '{print "  "$0}'
    fi
}

noHook () {
    return 0
}

howManyCommits () {
    echo -ne "($(command grep -o 'by [0-9]\+ commit' | sed -e 's/[^0-9]\+//g'))"
}

divergion () {
    echo -ne "($(sed -e '/and have/!d' -e 's/[^0-9 ]\+//g' -e 's/ \+/+/g' -e 's/^+//' -e 's/+$//' ))"
}

upstream () {
    echo -ne "$(git rev-parse --abbrev-ref --symbolic-full-name '@{u}')"
}

shortstat () {
    echo -ne "$( (timeout 0.1 git diff --shortstat "${@}" \
      || echo -ne "\e[0;33m git diff timeout! Blob change?\e[0m") \
      | sed -r -e 's/[[:digit:]]+ files? changed/\\e[0;34m\0\\e[0m/g' \
               -e 's/[[:digit:]]+ insertions?\(\+\)/\\e[0;32m\0\\e[0m/g' \
               -e 's/[[:digit:]]+ deletions?\(-\)/\\e[0;31m\0\\e[0m/g')"
}

diffstat () {
    shortstat "$(git whereishome)"
}

commitstat () {
    shortstat "--staged"
}

commandPromptWithMetadata () {
    local PC='\e[0m'
    local UPSTREAMC='\e[0;33m'

    local BRANCH='\e[1;32m'
    local PATHCOLOUR='\e[1;31m'

    local CHANGED='\e[1;33m'
    local UNTRACK='\e[1;34m'
    local TCOMMIT='\e[1;37m'

    local AHEAD='\e[0;31m'
    local BEHIND='\e[1;31m'

    local CLEAN='\e[0;32m'
    local STASH='\e[0;33m'

    echo -e "\n  --"

    local STATUS
    if STATUS=$(git status 2> /dev/null && git stash list 2> /dev/null)
    then
        local GIT_BRANCH
        GIT_BRANCH=$(__git_ps1 %s)
        echo -ne "  branch is: $BRANCH$GIT_BRANCH ($(git rev-parse --short HEAD 2> /dev/null))$PC"

        local GREPS=( )
        local LABELS=( )
        local HOOKS=( )

        GREPS[0]="^(# )?Changes not staged"
        LABELS[0]="$CHANGED#changed"

        GREPS[99]="^(# )?Unmerged paths"
        LABELS[99]="$BEHIND#unmerged"

        GREPS[100]="^(# )?Untracked files"
        LABELS[100]="$UNTRACK#untracked"

        GREPS[200]="to be committed"
        LABELS[200]="$TCOMMIT#uncommitted"

        GREPS[300]="branch is ahead"
        LABELS[300]="$AHEAD#ahead"
        HOOKS[300]="howManyCommits"

        GREPS[400]="branch is behind"
        LABELS[400]="$BEHIND#behind"
        HOOKS[400]="howManyCommits"

        GREPS[500]="can be fast-forwarded"
        LABELS[500]="$CLEAN#ff"

        GREPS[600]="have diverged"
        LABELS[600]="$CHANGED#diverged"
        HOOKS[600]="divergion"

        GREPS[700]="directory clean"
        LABELS[700]="$CLEAN#clean"

        GREPS[900]="^stash@"
        LABELS[900]="$STASH#stash"

        GREPS[990]="${GREPS[0]}"
        LABELS[990]="\n  ${PC}diff stat:${CLEAN}"
        HOOKS[990]="diffstat"

        GREPS[991]="${GREPS[200]}"
        LABELS[991]="\n  ${PC}to commit:${CLEAN}"
        HOOKS[991]="commitstat"

        GREPS[999]="(${GREPS[300]})|(${GREPS[400]})|(${GREPS[600]})"
        LABELS[999]="\n  ${PC}ustream's: ${UPSTREAMC}"
        HOOKS[999]="upstream"

        local i

        for i in ${!GREPS[*]}
        do
            if echo "$STATUS" | grep -E "${GREPS[$i]}" &> /dev/null
            then
                echo -ne " ${LABELS[$i]}"
                echo "$STATUS" | ${HOOKS[$i]:-noHook}
            fi
        done
    fi

    echo -ne "$PC\n"

    local HEADLINE="  we are at: "
    local colnumber="$(tput cols)"

    local currentPath="$(pwd)"

    if [[ $(( ${#HEADLINE} + ${#currentPath} )) -ge $colnumber ]]
    then
        currentPath="${currentPath//$GIT_HOME/\$GIT_HOME}"

        if [[ $(( ${#HEADLINE} + ${#currentPath} )) -ge $colnumber ]]
        then
            currentPath="really too big to show"
        fi
    fi

    echo -ne "$HEADLINE$PATHCOLOUR$currentPath$PC\n"

    randomlyShowFortune

    echo ""
}

callCommandPromptIfGit () {
    if GIT_HOME="$(git whereishome)" >& /dev/null
    then
        export GIT_HOME

        if ! grep -qsP "(^|:)${GIT_HOME}(:|$)" <(echo "$GIT_EXT_EXCLUSION")
        then
            commandPromptWithMetadata
        fi
    else
        unset GIT_HOME
    fi
}

commandPromptWithMetadataTime () {
    if [[ -n "$GIT_EXT_TIMED_PS" ]]
    then
        time callCommandPromptIfGit "${@}"
    else
        callCommandPromptIfGit "${@}"
    fi
}

gitExtrasCompletionInstall () {
    source "$GIT_EXT_INSTALL_PATH/completion"
}

ps1ctl::git () {
    brcd::prompt_command_add commandPromptWithMetadataTime
    export PS1='\u@\h \$ '
}

promptWithGit () {
  ps1ctl git
}

promptOriginal () {
    export PS1="${ORIGINAL_PS1}"
}

promptEnableRunTime () {
    export GIT_EXT_TIMED_PS=1
}

promptDisableRunTime () {
    unset GIT_EXT_TIMED_PS
}

git_extras_load () {
  if which git &> /dev/null
  then
    export GIT_EXT_INSTALL_PATH=$(dirname $(realpath "${BASH_SOURCE[0]}"))
    export ORIGINAL_PS1="${ORIGINAL_PS1:-$PS1}"

    # Evitando a poluir o PATH
    if ! echo "$PATH" | grep -qsP "(^|:)${GIT_EXT_INSTALL_PATH}(:|$)"
    then
      export PATH="${PATH}:${GIT_EXT_INSTALL_PATH}"

      if [ -d "${GIT_EXT_INSTALL_PATH}/local" ]
      then
          export PATH="${PATH}:${GIT_EXT_INSTALL_PATH}/local"
      fi
    fi

    gitExtrasCompletionInstall
    [[ -z $APPLY_PS1 ]] && { promptWithGit; }
  fi

  return 0
}

git_extras_load
